var fs = require('fs');
var path = require('path');
var util = require('util');
var http = require('http');
var https = require('https');
var process = require('process');
var models = require('./models');
var request = require('request');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

var DATA_DIR = "./data";
var HTTP_PORT = 5000;
var HTTPS_PORT = 5443;
var AUTHORIZED_TOKENS = ['haP2M4a1TyQFDxMqQL6/kjcytQZXuDYYmAy4n8jdIHg=']
var AUTHORIZED_CONTENT_TYPES = ['application/json', 'text/plain; charset=UTF-8']

// change to the SVC_HOME directory if specified
if (process.env.SVC_HOME) {
	console.log("changing to directory: " + process.env.SVC_HOME)
	process.chdir(process.env.SVC_HOME)
}

// create DATA_DIR if needed
if (!fs.existsSync(DATA_DIR)) {
	fs.mkdirSync(DATA_DIR)
}

var tls = {
	pfx: fs.readFileSync('./localhost.pfx'),
	passphrase: 'localhost'
};

// set express options
app.set('json spaces', 2)

// add middleware(s) to app
app.use(bodyParser.json())
app.use(bodyParser.text())

// define some routes
app.get('/messages', (req, res, next) => {
	models.messages.findAll().then((messages) => {
		res.json({ messages });
	});

});

app.get('/notifications', (req, res, next) => {
	var address = req.query.address || null;

	if(address) {
		models.notifications.getStatistics().then((stats) => {
			res.json({stats})
		})
	} else {
		models.notifications.findAll().then((notifications) => {
			res.json({ notifications });
		});
	}
});

app.get('/top', (req, res, next) => {
	var count = req.query.count || null;
	var days  = req.query.days  || null;
	models.notifications.topStats(count, days).then((notifications) => {
		res.json({ "bounceNotifications":notifications });
	});
});

app.get('/stats', (req, res, next) => {
	var address = req.query.address || null;
	var days    = req.query.days    || null;
	models.notifications.getStats(address, days).then((notifications) => {
		res.json({ "bounceNotifications":notifications });
	});
});

app.all('/*', (req, res, next) => {
	var msg = null;			// the fully parsed/processed/error-checked message
	var status = 200;		// default response status code
	var clientIp = req.ip;
	var contentType = req.get('Content-Type') || null;
	var method = req.method;
	var token = req.query.token || null;
	var logMsg = '';

	// format a log message
	logMsg = util.format("%s %s %s %s", clientIp, method, contentType, req.originalUrl);

	// if the request contains a null or invalid token, return 402 (forbidden)
	if (!token || AUTHORIZED_TOKENS.indexOf(token) === -1) {
		status = 403
		logMsg += (' ' + status);
		res.sendStatus(status).end();
		console.log(logMsg);
		return null;
	}

	// if the content-type of the request isn't listed in
	// AUTHORIZED_CONTENT_TYPES, return 400 (bad request)
	if (!contentType || AUTHORIZED_CONTENT_TYPES.indexOf(contentType) === -1) {
		status = 400
		logMsg += (' ' + status);
		res.sendStatus(status).end();
		console.log(logMsg)
		return null;
	}

	// AWS doesn't set the content type correctly so we have to manually parse
	// the body as JSON (wtf..amazon)
	if (contentType.includes('text/plain')) {
		msg = JSON.parse(req.body)
	}
	else if (contentType.includes('application/json')) {
		msg = req.body
	}

	// if the msg does not contain a 'type' attribute, return 400.
	if (!msg.Type) {
		status = 400
		logMsg += (' ' + status);
		res.sendStatus(status).end();
		console.log(logMsg)
		return null;
	}

	// handle the notification
	switch (msg.Type) {
		case 'Notification':
			// AWS encodes the 'Message' attribute as a string of JSON so we must
			// reconsitiute the data ourselves...
			if(typeof msg.Message === 'string') {
				msg.Message = JSON.parse(msg.Message)
			}

			var filename = path.join(DATA_DIR, msg.MessageId + '.json')
			fs.writeFile(filename, JSON.stringify(msg, null, 2), (err) => {
				if (err) throw err;
			});

			// store the raw message in the DB
			models.messages.create({
				id: msg.MessageId,
				type: msg.Type,
				json: msg
			});

			// parse and store the interesting attributes of the notification
			// in the db
			var recipients = msg.Message.bounce.bouncedRecipients
			for (var i = 0, len = recipients.length; i < len; i++) {
				models.notifications.create({
					id: msg.MessageId,
					type: msg.Message.notificationType,
					address: recipients[i].emailAddress,
					timestamp: msg.Message.bounce.timestamp
				});
			}
			// notification was successfully receieved and stored; return 200
			status = 200
			logMsg += (util.format(" %s %s", status, msg.Message.notificationType.toLowerCase()));
			res.sendStatus(status).end();
			console.log(logMsg)
			break;
		case 'SubscriptionConfirmation':
			request(msg.SubscribeURL, function (error, response, body) {
				if (!error) {
					status = 200
					logMsg += (' ' + status + ' subscribed ');
					res.sendStatus(status).end();
					console.log(logMsg)
				}
			});
			break;

		default:
			status = 400
			logMsg += (' ' + status);
			res.sendStatus(status).end();
			console.log(logMsg)
	}
});

function loadMessages(dir) {
	var files = fs.readdirSync(dir);

	for (var i = 0, len = files.length; i < len; i++) {
		file = path.join(dir, files[i])
		if (fs.statSync(file).isFile() && file.endsWith(".json")) {
			console.log('Processing: ' + file)
			fs.createReadStream(file).pipe(request.put('http://localhost:5000/?token='+AUTHORIZED_TOKENS[0], (err, res) => {
				if(err){
					console.log('error while posting file: ' + file)
				}
			}))
		}
	}
}

models.sequelize.sync().then(() => {
	console.log("Starting http server on " + HTTP_PORT)
	http.createServer(app).listen(HTTP_PORT);

	console.log("Starting https server on " + HTTPS_PORT)
	https.createServer(tls, app).listen(HTTPS_PORT);

	loadMessages(DATA_DIR);
})