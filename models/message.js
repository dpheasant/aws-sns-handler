"use strict";

module.exports = function (sequelize, DataTypes) {
    var Message = sequelize.define("messages",
        {   // attributes
            id  : { type: DataTypes.STRING, primaryKey: true },
            type: DataTypes.STRING,
            json: {
                type: DataTypes.TEXT,
                get: function () {
                    return JSON.parse(this.getDataValue('json'))
                },
                set: function (val) {
                    this.setDataValue('json',JSON.stringify(val))
                }
            }
        }, { // options

        });

    return Message;
};