"use strict";

module.exports = function (sequelize, DataTypes) {
    var Notification = sequelize.define("notifications", {
        id:      { type: DataTypes.STRING, unique: 'compositeKey', primaryKey: true },
        address: { type: DataTypes.STRING, unique: 'compositeKey', primaryKey: true },
        type:      DataTypes.STRING,
        timestamp: DataTypes.DATE
    }, {});

    Notification.getAll = () => {
        var query = {
            attributes: [
                'address',
                [sequelize.fn('MAX', sequelize.col('timestamp')), 'last'],
                [sequelize.fn('COUNT', sequelize.col('address')), 'count']
            ],
            group: 'address',
            order: [
                [sequelize.fn('COUNT', sequelize.col('address')), 'DESC']
            ]
        }
        return Notification.findAll(query);
    };

    // return a the top $count addresses that have bounced over the last
    // number of $days
    Notification.topStats = (count, days) => {
        count = count || 5
        days  = days  || 5

        var date = new Date();
        date.setDate(date.getDate() - days);
        date.setHours(0, 0, 0, 0);

        var query = {
            attributes: [
                'address',
                [sequelize.fn('MIN', sequelize.col('timestamp')), 'first'],
                [sequelize.fn('MAX', sequelize.col('timestamp')), 'last'],
                [sequelize.fn('COUNT', sequelize.col('address')), 'count'],
            ],
            where : { timestamp: { $gt: date } },
            group : 'address',
            order : [
                [sequelize.fn('COUNT', sequelize.col('address')), 'DESC']
            ],
            limit: count
        }
        
        return Notification.findAll(query)
    };

    Notification.getStats = (address, days) => {
        var query = {
            attributes: [
                'address',
                [sequelize.fn('MIN', sequelize.col('timestamp')), 'first'],
                [sequelize.fn('MAX', sequelize.col('timestamp')), 'last'],
                [sequelize.fn('COUNT', sequelize.col('address')), 'count'],
            ]
        }

        if (!address && !days) {
            // if both address and days are null, return the address,
            // bounce count, and timestamp of most recent bounce for all
            // addresses in the database (NOTE: could be resource intensive)
            query.group = 'address'
            query.order = [
                [sequelize.fn('COUNT', sequelize.col('address')), 'DESC']
            ]
        } else if (address && !days) {
            // if addresss is specified and days is null, return the
            // address, bounce count, and timestamp of most recent bounce
            // for the address
            query.where = sequelize.where(sequelize.fn('lower', sequelize.col('address')), address )

        } else if (!address && days) {
            // if address is null and days is specified, return the address,
            // address, bounce count, and timestamp of most recent bounce,
            // and period (in days) for all addresses in the database
            // (NOTE: could be resource intensive)
            var date = new Date();
            date.setDate(date.getDate() - days);
            date.setHours(0, 0, 0, 0);

            query.where = { timestamp: { $gt: date } };
            query.group = 'address'
            query.order = [
                [sequelize.fn('COUNT', sequelize.col('address')), 'DESC']
            ]

        } else if (address && days) {
            // if both address and days are specified, return the the
            // address, bounce count, timestamp of most recent bounce, and
            // period (in days)
            var date = new Date();
            date.setDate(date.getDate() - days);
            date.setHours(0, 0, 0, 0)

            query.where = [
                { address: address },
                { timestamp: { $gt: date } }
            ]
        } else {
            query = null;
        }

        if (query) {
            return Notification.findAll(query);
        } else {
            return null;
        }
    };

    Notification.getStatistics = () => {
        var query = {
            attributes: [
                'address',
                [sequelize.fn('MAX', sequelize.col('timestamp')), 'last'],
                [sequelize.fn('COUNT', sequelize.col('address')), 'count']
            ],
            group: 'address',
            order: [
                [sequelize.fn('COUNT', sequelize.col('address')), 'DESC']
            ]
        }
        return Notification.findAll(query);
    };

    return Notification;
};