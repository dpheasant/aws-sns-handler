'use strict'
var pjson = require('./package.json');
var util = require('util');
var path = require('path');

// grab some environmental information for later use
var platform  = process.platform;
var svcHome   = path.dirname(process.argv[1])
var svcConfig = {
    name:        'GeoDecisions ' + pjson.name,
    description: 'GeoDecisions ' + pjson.name,
    script:      path.join(svcHome, pjson.main),
    env : [{
        name: "SVC_HOME",
        value: svcHome
    }]
}

function usage() {
    console.log("USAGE: service.js <action>")
    console.log("")
    console.log("actions:")
    console.log("    install   - install this application as a native service")
    console.log("    uninstall - uninstall the application service definition")
}

// parse our arguments
if (process.argv.length == 3) {
    var action = process.argv[2];
} else {
    usage()
    process.exit(-1)
}

// detect the platform we're running on and require the proper node service-manager
switch (platform) {
    case 'win32':
        var Service = require('node-windows').Service;
        break;
    default:
        console.log(util.format('ERROR: %s is not supported on the %s platform', pjson.name, platform))
}

// Create a new service object
var svc = new Service(svcConfig);

// setup listeners for service-manager events
svc.on('install', function () {
    console.log(util.format('Successfully installed %s as a service', svcConfig.name))
});

svc.on('uninstall', function () {
    console.log(util.format('Successfully uninstalled the %s service', svcConfig.name))
});

svc.on('alreadyinstalled', function () {
    console.log(util.format('%s is already installed as a service', svcConfig.name))
});

// carry out the user specified action
switch (action) {
    case 'install':
        svc.install();
        break;

    case 'uninstall':
        svc.uninstall();
        break;

    case 'reinstall':
        svc.uninstall();
        svc.install();
        break;
    default:
        console.log(util.format("ERROR: unknown action '%s'",action))
}